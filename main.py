# -*- coding: utf-8 -*-
from os.path import dirname, join
import webapp2
import jinja2
from google.appengine.api import mail

CONTACT_EMAIL_TO = 'webadmin@accopensys.com'

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(join(dirname(__file__), 'templates')))

URL_TEMPLATE_MAP = {
    '':                         'about_us.html', 
    'application-development':  'application_development.html',
    'data-mining':              'data_mining.html',
    'system-integration':       'system_integration.html',
    'contact-us':               'contact_us.html',
    'privacy-statement':        'privacy_statement.html',
    'terms-of-use':             'terms_of_use.html',
}
                    
class PageHandler(webapp2.RequestHandler):
    def get(self, slug):
        if URL_TEMPLATE_MAP.has_key(slug):
            template = JINJA_ENVIRONMENT.get_template(
                join('pages', URL_TEMPLATE_MAP[slug]))
            self.response.headers['Content-Type'] = 'text/html'
            self.response.write(template.render({'slug': slug}))
        else:
            template = JINJA_ENVIRONMENT.get_template('404.html')
            self.error(404)
            self.response.write(template.render())
            
    def post(self, slug):
        if URL_TEMPLATE_MAP.has_key(slug):
            mail.send_mail(
                sender       = 'Accopensys website <accopensys-com@appspot.gserviceaccount.com>',
                to           = CONTACT_EMAIL_TO,
                subject      = 'New message reveived at www.accopensys.com',
                body         = 'From: %s <%s>\nMessage:\n%s' % (
                                    self.request.get("name"),
                                    self.request.get("email"),
                                    self.request.get("comment")))
            self.redirect('/%s/' % slug)
        else:
            template = JINJA_ENVIRONMENT.get_template('404.html')
            self.error(404)
            self.response.write(template.render())
                          
application = webapp2.WSGIApplication([
    ('^/(?P<slug>[\w-]*)/?$', PageHandler),
    ('^(?P<slug>.*)$', PageHandler),
], debug=False)

